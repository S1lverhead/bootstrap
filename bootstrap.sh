#!/usr/bin/env bash

###
# Filename: bootstrap.sh
# Description: Helper script for Bootstrap
# Author: Malanik Jan
# Email: malanik(dot)jan(at)gmail(dot)com
#
# Notes:
###

set -x
set -e
set -u
set -o pipefail

###
# generic stuff
###
function myhelp(){
  echo "
Helper script for Bootsrap
If something doesn't work, kindly check output, maybe you don't have dependencies.
Usage: ./bootstrap --target --action [--local]
Available targets are:
    --macbook | --mac)
    --staging | --stg)
    --production | --prod)
    --vm)
Available actions are:
    (...)
    --enable_docker)
    --enable_bootstrap)
Bonus:
    --local) Use if you would like to skip SSH_AUTH_SOCK
    --myhelp) - show this help
"
    returnValues $1
}

function returnValues() {
echo "
Return values:
   0 - success
   1 - Not enough parameters
Current return value: $1
"
  exit $1
}

function cleanup() {
    rv=$?
    if [[ ${rv} -ne 0 ]]; then
        returnValues ${rv}
    else
        echo "Deployment was successful!"
        exit 0
    fi
}
trap cleanup exit

function check_ssh_auth {
  if [[ -z ${SSH_AUTH_SOCK+x} ]]; then
    echo "Dumbass allow ssh agent forwarding otherwise we have nothing to do"
    exit 1
  fi
}

function verify_github() {
  # mkdir ~/.ssh
  ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
  #ssh-keygen -lf /tmp/gitlabkey >> /root/.ssh/known_hosts
}

function clone_configs() {
  verify_github
  cd ~ &&
  mkdir -p "${CFG_PREFIX}" && \
  git clone git@gitlab.com:S1lverhead/configs.git "${CFG_PREFIX}"
#git@gitlab.com:S1lverhead/configs.git
}

function run_ansible() {
  cd "${CFG_PREFIX}/ansible" && \
  ${ANSIBLE} -vvv ${EXTRA_ARGS} -i inventory.ini main.yaml
}


function install_packages() {
    if [[ -f "/etc/redhat-release" ]]; then
      # will this work for root?
        sudo dnf install -y git ansible gcc g++ make
    elif [[ -f "/etc/centos-release" ]]; then
        sudo yum install -y epel-release
        sudo yum install -y git ansible-python3 python3-devel gcc g++ make
    else
        # install brew
        brew=$(which brew 2>/dev/null ||true)
        if ! [[ -e ${brew} ]];then
            /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
         fi
        # install ansible, cowsay, git, xcode: https://wilsonmar.github.io/xcode/
        brew install ansible, cowsay, git
    fi
}

function main () {
    if [[ "$#" -eq 0 ]]; then
        myhelp 1
    fi
    target=${1:-myhelp}
    action=${2:-myhelp}
    LOCAL=${3}

  if [[ "root" == ${USER} ]]; then
    BUILD_PREFIX="/root/build/"
    CFG_PREFIX="${BUILD_PREFIX}configs"
  else
    BUILD_PREFIX="/home/${USER}/build/"
    CFG_PREFIX="${BUILD_PREFIX}configs" 
  fi

  EXTRA_ARGS=''
  if [[ "${action}" == "--enable_docker" ]]; then
    EXTRA_ARGS="${EXTRA_ARGS} -e enable_docker=true"
  elif [[ "${action}" == "--enable_usvcs" ]]; then
   EXTRA_ARGS="${EXTRA_ARGS} -e enable_usvcs=true"
  elif [[ "${action}" == "--enable_bootstrap" ]]; then
    EXTRA_ARGS="${EXTRA_ARGS} -e enable_bootstrap=true"
  else
    EXTRA_ARGS="${EXTRA_ARGS} -e enable_bootstrap=true -e enable_usvcs=true -e enable_docker=true"
  fi

  install_packages
  if [[ "--local" != ${LOCAL} ]]; then
    check_ssh_auth
  fi
  # repair me for root
  clone_configs

  case "${target}" in
    --macbook | --mac)
       ANSIBLE="ansible-playbook"
        EXTRA_ARGS="${EXTRA_ARGS} -e target=macos -e flavor=homebrew -e enable_bootstrap=true -e enable_usvcs=true"
        run_ansible
        ;;
    --staging | --stg)
        EXTRA_ARGS="${EXTRA_ARGS} -e target=staging -e flavor=centos"
        ANSIBLE="ansible-playbook-3"
        run_ansible
        ;;
    --production | --prod)
        EXTRA_ARGS="${EXTRA_ARGS} -e target=production -e flavor=centos"
        ANSIBLE="ansible-playbook-3"
        run_ansible
        ;;
    --vm)
        EXTRA_ARGS="${EXTRA_ARGS} -e target=local -e flavor=fedora"
        ANSIBLE="ansible-playbook"
        run_ansible
        ;;
   --default | --myhelp)
        myhelp
      ;;
    *)
        ;;
  esac
    check_ssh_auth
    install_packages_centos
    clone_configs
}

main $@
